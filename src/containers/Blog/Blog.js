import React, { Component } from 'react';

import './Blog.css'

import Post from '../../Components/Post/Post';
import FullPost from '../../Components/FullPost/FullPost';
import NewPost from '../../Components/NewPost/NewPost';

class Blog extends Component {
  render() {
    return (
      <div>
        <section className='recentPosts'>
          <Post />
          <Post />
          <Post />
        </section>
        <section className='detailPost'>
          <FullPost />
        </section>
        <section className='newPost'>
          <NewPost />
        </section>
      </div>
    );
  }
}

export default Blog;
