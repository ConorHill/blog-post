import React from 'react'

const postTitle = props => (
    <h3>{props.children}</h3>
);

export default postTitle;