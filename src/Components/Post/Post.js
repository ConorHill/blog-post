import React from 'react'

import './Post.css';
import PostTitle from './PostTitle/PostTitle';
import PostAuthor from './PostAuthor/PostAuthor';

const post = (props) => {
    // ...

    return (
        <div className='postContainer'>
            <PostTitle>Title</PostTitle>
            <PostAuthor>Author</PostAuthor>
        </div>
    )

}

export default post;