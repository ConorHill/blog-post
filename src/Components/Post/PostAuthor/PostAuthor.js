import React from 'react'

const postAuthor = props => (
    <h4>{props.children}</h4>
)

export default postAuthor;