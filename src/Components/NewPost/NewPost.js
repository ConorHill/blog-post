import React, { Component } from 'react'

import './NewPost.css'
export default class NewPost extends Component {
    render() {
        return (
            <form>
            <div className="newPostContainer">
                <h3 className="newPostTitle">Add a New Post</h3>
                <label className="label" for="postTitle">Title</label>
                <input id="postTilte" type="text" />
                <label className="label" for="postContent">Content</label>
                <textarea id="postContent"></textarea>
                <label className="label" for="postAuthor">Author</label>
                <select id="postAuthor">
                    <option value="Conor">Conor</option>
                    <option value="Laura">Laura</option>
                    <option value="Darren">Darren</option>
                </select>
                <button type="submit">Add Post</button>
            </div>

            </form>
        )
    }
}
