import React, { Component } from 'react'

class FullPost extends Component {
    render() {
        let post = <h3>Please select a post!</h3>;

        post = (
            <div className='PostDetail'>
                <h3 className='PostTitle'>Title</h3>
                <p className='PostContent'>Some Content</p>
                <button className='btn'>Delete</button>
            </div>
        )
        return post;
    }
}

export default FullPost;